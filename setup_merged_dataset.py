import os
import cv2
import numpy as np
import matplotlib.pyplot as plt

composite_dir = "/Users/eunjae/uiuc/fa19/cs445/cs445_final/datasets/composite/test"

if not os.path.isdir(composite_dir):
    os.mkdir(composite_dir)

# It merges two images side by side.

background_A = cv2.imread("/Users/eunjae/uiuc/fa19/cs445/cs445_final/datasets/source/case2/0_real_A.png")
background_B = cv2.imread("/Users/eunjae/uiuc/fa19/cs445/cs445_final/datasets/source/case2/0_fake_B.png")
foreground_A = cv2.imread("/Users/eunjae/uiuc/fa19/cs445/cs445_final/datasets/source/case2/1_real_A.png")
foreground_B = cv2.imread("/Users/eunjae/uiuc/fa19/cs445/cs445_final/datasets/source/case2/1_fake_B.png")

print(foreground_A.shape)
print(foreground_B.shape)

background = np.hstack((background_A, background_B))
foreground = np.hstack((foreground_A, foreground_B))

# cv2.imwrite(os.path.join(composite_dir, "0_real_fake.jpg"), background)
# cv2.imwrite(os.path.join(composite_dir, "1_real_fake.jpg"), foreground)

# It changes the format of the composite image.

for i in range(3,11):
	print(i)
	composite_B = cv2.imread("/Users/eunjae/uiuc/fa19/cs445/cs445_final/datasets/source/case3/merged" + str(i) + ".png")
	composite = np.zeros(background.shape, dtype=np.uint8)
	composite[:,256:512,:] = composite_B

	cv2.imwrite(os.path.join(composite_dir, "composite" + str(i) + ".jpg"), composite)	

# plt.figure()
# plt.imshow(composite)
# plt.show()

# print(composite.shape)

